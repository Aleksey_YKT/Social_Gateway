//
//  NavigationViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 30.11.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationBar.barTintColor = UIColor(red: 47.0/255.0, green: 78.0/255.0, blue: 129.0/255.0, alpha: 1.0)
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
