//
//  LoginViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 30.11.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginTextField: UITextField!
    
    var needToHideBackButtom = false
    
    var userID = ""
    var userEmail = ""
    var userLogin = ""
    var userPassword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (needToHideBackButtom) {
            self.navigationItem.setHidesBackButton(true, animated: true)
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func confirmButtonPressed(_ sender: AnyObject) {
        view.endEditing(true)
        
        userLogin = loginTextField.text!
        userPassword = MD5(passwordTextField.text!)
        
        if (userLogin != "" && userPassword != "") {
            login(userLogin: userLogin, userPassword: userPassword)
        } else {
            let alertMessage = UIAlertController(title: "Ошибка", message: "Заполните поля", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertMessage, animated: true, completion: nil)
        }
    }
    
    func login(userLogin: String, userPassword: String) {
        let requestParams = "name=" + userLogin + "&password=" + userPassword
        
        var request = URLRequest(url: URL(string: getUsersURL)!)
        request.httpMethod = "POST"
        let postString = requestParams
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Ошибка на сервере \(error)")
                
                let alertMessage = UIAlertController(title: "Ошибка", message: "Ошибка на сервере \(error)", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("Ошибка на сервере \(httpStatus.statusCode)")
                
                let alertMessage = UIAlertController(title: "Ошибка", message: "Ошибка на сервере \(httpStatus.statusCode)", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                print("Ваш запрос отправлен")
                
                let responseString = String(data: data, encoding: .utf8)
                
                if (responseString != "[]") {
                    DispatchQueue.main.async {
                        DispatchQueue.main.async {
                            self.parseJSON((responseString?.data(using: .utf8))!)
                            self.saveUserData()
                            
                            self.performSegue(withIdentifier: "openMainView", sender: self)
                        }
                    }
                } else {
                    let alertMessage = UIAlertController(title: "Ошибка", message: "Такого пользователя не существует или неверный пароль", preferredStyle: .alert)
                    alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alertMessage, animated: true, completion: nil)
                }
                
                print(responseString as String!)
            }
        }
        
        task.resume()
    }
    
    func saveUserData() {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(appUserFile)
            var userData = [String: String]()
            userData["id"] = userID
            userData["login"] = userLogin
            userData["email"] = userEmail
            userData["password"] = userPassword
            
            var responseString = userData.description
            responseString.remove(at: responseString.startIndex)
            responseString.remove(at: responseString.index(before: responseString.endIndex))
            responseString = "{" + responseString + "}"
            print(responseString)
            
            // Сохранение в файл
            do {
                try responseString.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                print("Файл сохранен")
            } catch {
                print("Ошибка при сохранении в файл")
            }
        }
    }
    
    func parseJSON(_ data: Data) {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert posts to JSON")
                return
            }
            
            if let userID_T = json["id"] as? String {
                userID = userID_T
            }
            
            if let userEmail_T = json["email"] as? String {
                userEmail = userEmail_T
            }
        } catch {
            print(error)
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
