//
//  SettingsViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 02.12.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit
import SwiftyVK

class SettingsViewController: UIViewController {

    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var vkLoginButton: UIButton!
    @IBOutlet weak var twitterLoginButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    var userVKData = String()
    var userVKToken = String()
    var userVKID = String()
    var userVKEmail = String()
    
    var userVKFirstName = String()
    var userVKLastName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userVKData = loadDataFromFile(vkTokenFile)
        let userTwitterToken = loadDataFromFile(twitterTokenFile)
        
        userNameLabel.text = userTwitterToken
        
        if (userVKData != "") {
            parseJSON(userVKData.data(using: .utf8)!)
            
            vkLoginButton.setTitle("Выйти из ВК", for: UIControlState.normal)
        } else {
            vkLoginButton.setTitle("Войти в ВК", for: UIControlState.normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logoutFromApp(_ sender: Any) {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(appUserFile)
            let responseString = ""
            
            // Сохранение в файл
            do {
                try responseString.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                print("Файл сохранен")
            } catch {
                print("Ошибка при сохранении в файл")
            }
        }
        
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "backToStartView", sender: self)
            }
        }
    }
    
    func parseJSON(_ data: Data) {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert posts to JSON")
                return
            }
            
            if let userToken = json["access_token"] as? String {
                userVKToken = userToken
            }
            
            if let userID = json["user_id"] as? String {
                userVKID = userID
            }
            
            if let userEmail = json["email"] as? String {
                userVKEmail = userEmail
            }
        } catch {
            print(error)
        }
    }
    
    func loadDataFromFile(_ fileName: String) -> String {
        var userTokenFile = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(fileName)
            
            //reading
            do {
                userTokenFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
            } catch {
                print("Ошибка при чтении с файла")
            }
        }
        
        return userTokenFile
    }
}
