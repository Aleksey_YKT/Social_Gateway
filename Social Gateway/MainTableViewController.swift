//
//  MainViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 05.11.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {
    
    var userID = ""
    var userEmail = ""
    var userLogin = ""
    var userPassword = ""

    var posts = [Post]()
    var postsOffset = 20
    var subscribedProviders = [Provider]()
    var provider = [Provider]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        parseJSON(loadDataFromFile().data(using: .utf8)!)
        getDataFromServer()
        getSubscribedProvidersFromServer()
        getProvidersFromServer()
        
        self.refreshControl?.addTarget(self, action: #selector(MainTableViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func handleRefresh(_ refreshControl: UIRefreshControl) {
        userID = ""
        userEmail = ""
        userLogin = ""
        userPassword = ""
        
        posts = [Post]()
        postsOffset = 20
        parseJSON(loadDataFromFile().data(using: .utf8)!)
        getDataFromServer()
        
        self.tableView.reloadData()
        
        refreshControl.endRefreshing()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mainTableCell", for: indexPath) as! MainTableViewCell
        var url = ""
        
        if posts[(indexPath as NSIndexPath).row].attachments_link != "" {
            url = posts[(indexPath as NSIndexPath).row].attachments_link
        } else {
            url = emptyPhotoURL
        }
        
        if let checkedUrl = URL(string: url) {
            getImageFromUrl(url: checkedUrl) { (data, response, error) in guard let data = data, error == nil else  { return }
                DispatchQueue.main.async() { () -> Void in
                    cell.postImageView?.image = UIImage(data: data)
                }
            }
        }
        
        cell.postProviderNameLabel?.text = posts[(indexPath as NSIndexPath).row].provider_title
        cell.postTimeLabel?.text = CustomDateFormatter(postTime: posts[(indexPath as NSIndexPath).row].post_ts)
        cell.postNameLabel?.text = posts[(indexPath as NSIndexPath).row].post_text
        
        if (indexPath as NSIndexPath).row + 1 == postsOffset - 20 {
            getDataFromServer()
        }
        
        return cell
    }

    func getImageFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    // Открытие следующего view - DetailsViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    
        if segue.identifier == "showPostDetails" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! DetailsViewController
                destinationController.post = posts[(indexPath as NSIndexPath).row]
                tableView.deselectRow(at: indexPath, animated: false)
            }
        } else if segue.identifier == "openSubscribesView" {
            let destinationController = segue.destination as! SubscribesTableViewController
            destinationController.subscribedProviders = subscribedProviders
            destinationController.provider = provider
        }
    }
    
    
    // Получение JSON файла из сервера
    func getDataFromServer() {
        let getPostsRequestURLWithParameters = getPostsRequestURL + "/" + userID + "/" + MD5(userID) + "/" + String(postsOffset)
        postsOffset = postsOffset + 20
        
        let request = URLRequest(url: URL(string: getPostsRequestURLWithParameters)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parsePostsData(data)
                
                OperationQueue.main.addOperation({ () -> Void in
                    self.tableView.reloadData()
                })
            }
            
        })
        
        task.resume()
    }
    
    // Парсинг полученной JSON
    func parsePostsData(_ data: Data) {
        do {
            guard let postsData = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary] else {
                print("error trying to convert posts to JSON")
                return
            }
            
            for postData in postsData {
                let temp_post = Post()
                
                temp_post.id = postData["id"] as! String
                temp_post.post_text = postData["post_text"] as! String
                temp_post.pinned = postData["pinned"] as! Bool
                temp_post.post_ts = postData["post_ts"] as! String
                temp_post.provider_id = postData["provider_id"] as! String
                temp_post.user_id = postData["user_id"] as! String
                
                if let attch = postData["attachments"] as? NSArray {
                    if let attachment = attch[0] as? NSDictionary {
                        if let attch_id = attachment["id"] as? String {
                            temp_post.attachments_id = attch_id
                            
                            if let attch_content_type = attachment["content_type"] as? String {
                                temp_post.attachments_content_type = attch_content_type
                            }
                            
                            if let attch_link = attachment["link"] as? String {
                                temp_post.attachments_link = attch_link
                            }
                        }
                    }
                }
                
                if let provider = postData["provider"] as? NSDictionary {
                    temp_post.provider_id = provider["id"] as! String
                    
                    
                    if let provider_source_link = provider["source_link"] as? String {
                        temp_post.provider_source_link = provider_source_link
                    }
                    
                    temp_post.provider_title = provider["title"] as! String
                    
                    if let provider_description = provider["description"] as? String {
                        temp_post.provider_description = provider_description
                    }
                    
                    if let provider_logo = provider["logo"] as? String {
                        temp_post.provider_logo = provider_logo
                    }
                }
                
                if let service = postData["service"] as? NSDictionary {
                    temp_post.service_name = service["name"] as! String
                    
                    if let serivce_logo = service["logo"] as? String {
                        temp_post.service_logo = serivce_logo
                    }
                }
            
                posts.append(temp_post)
            }
            
        } catch {
            print(error)
        }
    }
    
    func loadDataFromFile() -> String {
        var userFile = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(appUserFile)
            
            //reading
            do {
                userFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
            } catch {
                print("Ошибка при чтении с файла")
            }
        }
        
        return userFile
    }
    
    
    func parseJSON(_ data: Data) {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert posts to JSON")
                return
            }
            
            if let userID_T = json["id"] as? String {
                userID = userID_T
            }
            
            if let userEmail_T = json["email"] as? String {
                userEmail = userEmail_T
            }
            
            if let userLogin_T = json["login"] as? String {
                userLogin = userLogin_T
            }
            
            if let userPassword_T = json["password"] as? String {
                userPassword = userPassword_T
            }
        } catch {
            print(error)
        }
    }
    
    //
    // Получение только тех, на кого подписан пользователь
    //
    func getSubscribedProvidersFromServer() {
        let requestParams = "/" + userID + "/" + MD5(userID)
        var request = URLRequest(url: URL(string: getSubscribedProvidersURL + requestParams)!)
        
        request.httpMethod = "POST"
        
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseSubscribedProvidersData(data)
                
                OperationQueue.main.addOperation({ () -> Void in
                    self.tableView.reloadData()
                })
            }
            
        })
        
        task.resume()
    }
    
    func parseSubscribedProvidersData(_ data: Data) {
        do {
            guard let providerData = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary] else {
                print("error trying to convert posts to JSON")
                return
            }
            
            for prov in providerData {
                let tempprov = Provider()
                
                if let prvdr = prov["provider"] as? NSDictionary {
                    tempprov.id = prvdr["id"] as! String
                    tempprov.title = prvdr["title"] as! String
                    
                    if let logo = prvdr["logo"] as? String {
                        tempprov.logo = logo
                    }
                    
                    if let description = prvdr["description"] as? String {
                        tempprov.description = description
                    }
                    
                    if let source_link = prvdr["source_link"] as? String {
                        tempprov.source_link = source_link
                    }
                }
                
                if let service = prov["service"] as? NSDictionary {
                    tempprov.service_name = service["name"] as! String
                    
                    if let logo = service["logo"] as? String {
                        tempprov.service_logo = logo
                    }
                }
                
                subscribedProviders.append(tempprov)
            }
        } catch {
            print(error)
        }
    }
    
    //
    //  Получение всех провайдеров
    //
    func getProvidersFromServer() {
        let request = URLRequest(url: URL(string: getPostsProviderRequestURL)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseProviderData(data)
                
                OperationQueue.main.addOperation({ () -> Void in
                    self.tableView.reloadData()
                })
            }
            
        })
        
        task.resume()
    }
    
    func parseProviderData(_ data: Data) {
        do {
            guard let providerData = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary] else {
                print("error trying to convert posts to JSON")
                return
            }
            
            for prov in providerData {
                let tempprov = Provider()
                
                tempprov.id = prov["id"] as! String
                tempprov.title = prov["title"] as! String
                
                if let logo = prov["logo"] as? String {
                    tempprov.logo = logo
                }
                
                if let description = prov["description"] as? String {
                    tempprov.description = description
                }
                
                if let source_link = prov["source_link"] as? String {
                    tempprov.source_link = source_link
                }
                
                if let service = prov["service"] as? NSArray {
                    if let ser = service[0] as? NSDictionary {
                        tempprov.service_name = ser["name"] as! String
                        
                        if let logo = ser["logo"] as? String {
                            tempprov.service_logo = logo
                        }
                    }
                }
                
                provider.append(tempprov)
            }
        } catch {
            print(error)
        }
    }
}
