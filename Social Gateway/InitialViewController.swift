//
//  InitialViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 02.12.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    var userID = ""
    var userEmail = ""
    var userLogin = ""
    var userPassword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userData = loadDataFromFile()
        
        if (userData != "") {
            DispatchQueue.main.async {
                DispatchQueue.main.async {
                    self.parseJSON(userData.data(using: .utf8)!)
                    self.login()
                }
            }
        } else {
            DispatchQueue.main.async {
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "showStartView", sender: self)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func login() {
        let requestParams = "name=" + userLogin + "&password=" + userPassword
        
        var request = URLRequest(url: URL(string: getUsersURL)!)
        request.httpMethod = "POST"
        let postString = requestParams
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Ошибка на сервере \(error)")
                
                let alertMessage = UIAlertController(title: "Ошибка", message: "Ошибка на сервере \(error)", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("Ошибка на сервере \(httpStatus.statusCode)")
                
                let alertMessage = UIAlertController(title: "Ошибка", message: "Ошибка на сервере \(httpStatus.statusCode)", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                print("Ваш запрос отправлен")
                
                let responseString = String(data: data, encoding: .utf8)
                
                if (responseString != "[]") {
                    DispatchQueue.main.async {
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "showMainView", sender: self)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "showStartView", sender: self)
                        }
                    }
                }
            }
        }
        
        task.resume()
    }
    
    func loadDataFromFile() -> String {
        var userFile = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(appUserFile)
            
            //reading
            do {
                userFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
            } catch {
                print("Ошибка при чтении с файла")
            }
        }
        
        return userFile
    }
    
    
    func parseJSON(_ data: Data) {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert posts to JSON")
                return
            }
            
            if let userID_T = json["id"] as? String {
                userID = userID_T
            }
            
            if let userEmail_T = json["email"] as? String {
                userEmail = userEmail_T
            }
            
            if let userLogin_T = json["login"] as? String {
                userLogin = userLogin_T
            }
            
            if let userPassword_T = json["password"] as? String {
                userPassword = userPassword_T
            }
        } catch {
            print(error)
        }
    }
}
