//
//  Post.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 28.11.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

class Post {
    var id: String = ""
    var post_text: String = ""
    var pinned: Bool = false
    var post_ts: String = ""
    var post_id: String = ""
    var user_id: String = ""
    
    var attachments_id: String = ""
    var attachments_content_type: String = ""
    var attachments_link: String = ""
    
    var provider_id: String = ""
    var provider_logo: String = ""
    var provider_description: String = ""
    var provider_source_link: String = ""
    var provider_title: String = ""
    
    var service_name: String = ""
    var service_logo: String = ""
}
