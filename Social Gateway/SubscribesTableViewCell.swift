//
//  SubscribesTableViewCell.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 05.12.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class SubscribesTableViewCell: UITableViewCell {

    @IBOutlet weak var providerStatusLabel: UILabel!
    @IBOutlet weak var providerLogoView: UIImageView!
    @IBOutlet weak var providerNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
