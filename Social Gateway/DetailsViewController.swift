//
//  DetailsViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 28.11.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var postSourceLinkTextView: UITextView!
    @IBOutlet weak var postServiceLabel: UILabel!
    @IBOutlet weak var postProviderLabel: UINavigationItem!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postTimeLabel: UILabel!
    @IBOutlet weak var postText: UITextView!
    
    var post = Post()
    var provider = Provider()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postTimeLabel.text = CustomDateFormatter(postTime: post.post_ts)
        postText.text = post.post_text
        postServiceLabel.text = post.service_name
        postProviderLabel.title = post.provider_title
        postSourceLinkTextView.text = post.provider_source_link
        
        if (post.attachments_link != "") {
            downloadImage(url: post.attachments_link)
        } else {
            downloadImage(url: emptyPhotoURL)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // Загрузка изображений из сервера
    func downloadImage(url: String) {
        
        if let checkedUrl = URL(string: url) {
            getImageFromUrl(url: checkedUrl) { (data, response, error)  in guard let data = data, error == nil else { return }
                DispatchQueue.main.async {
                    DispatchQueue.main.async {
                        self.postImageView.image = UIImage(data: data)
                    }
                }
            }
        }
    }
    
    func getImageFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
}
