//
//  CustomDateFormatter.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 02.12.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

func CustomDateFormatter(postTime: String) -> String {
    var formattedServiceTime = String()
    
    if (postTime.characters.count > 0) {
        let dateString = postTime
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "ru_RU_POSIX") as Locale!
        // 2016-11-09 06:04:31
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateObj = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "d MMMM HH:mm"
        formattedServiceTime = dateFormatter.string(from: dateObj!)
    }
    
    return formattedServiceTime
}
