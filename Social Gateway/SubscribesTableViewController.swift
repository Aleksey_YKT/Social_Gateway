//
//  SubscribesTableViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 05.12.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class SubscribesTableViewController: UITableViewController {

    var userID = ""
    var provider = [Provider]()
    var subscribedProviders = [Provider]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        parseJSON(loadDataFromFile().data(using: .utf8)!)
        updateSubs()
        
        let readyButton = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(backToMainView(sender:)))
        
        let addButton = UIBarButtonItem(title: "Добавить", style: .plain, target: self, action: #selector(addProviderView(sender:)))
        
        self.navigationItem.leftBarButtonItem = readyButton
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    func updateSubs() {
        for subProv in subscribedProviders {
            for prov in provider {
                if prov.id == subProv.id {
                    prov.user_subscribed = true
                }
            }
        }
    }
    
    func getIndexOfDelSub(prov: Provider) -> Int {
        var index = -1
        
        for i in 0...subscribedProviders.count - 1 {
            if subscribedProviders[i].id == prov.id {
                index = i
            }
        }
        
        return index
    }

    func backToMainView(sender: UIBarButtonItem) {
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "backToMainView", sender: self)
            }
        }
    }
    
    func addProviderView(sender: UIBarButtonItem) {
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "showAddProviderView", sender: self)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return provider.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subscribesTableCell", for: indexPath) as! SubscribesTableViewCell
        
        var url = ""
        
        if provider[(indexPath as NSIndexPath).row].logo != "" {
            url = provider[(indexPath as NSIndexPath).row].logo
        } else {
            url = emptyPhotoURL
        }
        
        if let checkedUrl = URL(string: url) {
            getImageFromUrl(url: checkedUrl) { (data, response, error) in guard let data = data, error == nil else  { return }
                DispatchQueue.main.async() { () -> Void in
                    cell.providerLogoView?.image = UIImage(data: data)
                }
            }
        }
        
        cell.providerNameLabel?.text = provider[(indexPath as NSIndexPath).row].title + " (" + provider[(indexPath as NSIndexPath).row].service_name + ")"
        
        if (provider[(indexPath as NSIndexPath).row].user_subscribed) {
            cell.providerStatusLabel?.text = checkMark
        } else {
            cell.providerStatusLabel?.text = ""
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let optionMenu = UIAlertController(title: nil, message: "Что вы хотите сделать?", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        let subscribeAction = UIAlertAction(title: "Подписаться", style: .default, handler: { (action: UIAlertAction!) -> Void in
            
            self.subuscribeToProvider(providerID: self.provider[(indexPath as NSIndexPath).row].id, reqURL: subscribeToProviderURL)
            
            self.subscribedProviders.append(self.provider[(indexPath as NSIndexPath).row])
            
            self.updateSubs()
             
        })
        
        let unsubscribeAction = UIAlertAction(title: "Отписаться", style: .default, handler: { (action: UIAlertAction!) -> Void in
            
            self.subuscribeToProvider(providerID: self.provider[(indexPath as NSIndexPath).row].id, reqURL: unsubscribeToProviderURL)
            
            self.subscribedProviders.remove(at: self.getIndexOfDelSub(prov: self.provider[(indexPath as NSIndexPath).row]))
            self.provider[(indexPath as NSIndexPath).row].user_subscribed = false
            
            self.updateSubs()
        })
        
        optionMenu.addAction(cancelAction)
            
        if (provider[(indexPath as NSIndexPath).row].user_subscribed) {
            optionMenu.addAction(unsubscribeAction)
        } else {
            optionMenu.addAction(subscribeAction)
        }
        
        self.present(optionMenu, animated: true, completion: nil)
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func subuscribeToProvider(providerID: String, reqURL: String) {
        let requestParams = "provider_id=" + providerID + "&user_id=" + userID + "&token=" + MD5(providerID + userID)
        var request = URLRequest(url: URL(string: reqURL)!)
        
        request.httpMethod = "POST"
        let postString = requestParams
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let _ = data, error == nil else {
                // check for fundamental networking error
                print("Ошибка на сервере \(error)")
                
                let alertMessage = UIAlertController(title: "Ошибка", message: "Ошибка на сервере \(error)", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("Ошибка на сервере \(httpStatus.statusCode)")
                
                let alertMessage = UIAlertController(title: "Ошибка", message: "Ошибка на сервере \(httpStatus.statusCode)", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                if (reqURL == subscribeToProviderURL) {
                    let alertMessage = UIAlertController(title: "Готово", message: "Вы подписались", preferredStyle: .alert)
                    alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alertMessage, animated: true, completion: nil)
                    
                    
                } else {
                    let alertMessage = UIAlertController(title: "Готово", message: "Вы отписались", preferredStyle: .alert)
                    alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alertMessage, animated: true, completion: nil)
                }
            }
        }
        
        self.tableView.reloadData()
        task.resume()
    }
    
    func getImageFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    //
    // Загрузка данных пользователя с файла
    //
    func loadDataFromFile() -> String {
        var userFile = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(appUserFile)
            
            //reading
            do {
                userFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
            } catch {
                print("Ошибка при чтении с файла")
            }
        }
        
        return userFile
    }
    
    
    func parseJSON(_ data: Data) {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert posts to JSON")
                return
            }
            
            if let userID_T = json["id"] as? String {
                userID = userID_T
            }
        } catch {
            print(error)
        }
    }
}
