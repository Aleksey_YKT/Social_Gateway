//
//  MainViewCell.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 05.11.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var postProviderNameLabel: UILabel!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postTimeLabel: UILabel!
    @IBOutlet weak var postNameLabel: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
