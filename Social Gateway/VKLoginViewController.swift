//
//  VKLoginViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 03.12.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit
import SwiftyVK

class VKLoginViewController: UIViewController, VKDelegate {
    
    let scope: Set<VK.Scope> = [.groups,.offline,.email,.friends,.photos,.video]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        VK.config.logToConsole = true
        VK.configure(withAppId: appID, delegate: self)
        
        if (VK.state == VK.States.authorized) {
            VK.logOut()
        } else {
            VK.logIn()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func vkWillAuthorize() -> Set<VK.Scope> {
        //Called when SwiftyVK need authorization permissions.
        return scope
    }
    
    func vkDidAuthorizeWith(parameters: Dictionary<String, String>) {
        //Called when the user is log in.
        //Here you can start to send requests to the API.
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(vkTokenFile)
            var responseString = parameters.description
            responseString.remove(at: responseString.startIndex)
            responseString.remove(at: responseString.index(before: responseString.endIndex))
            responseString = "{" + responseString + "}"
            
            // Сохранение в файл
            do {
                try responseString.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                print("Токен сохранен")
            } catch {
                print("Ошибка при сохранении в файл")
            }
        }
        
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "backToSettingsView", sender: self)
            }
        }
    }
    
    func vkAutorizationFailedWith(error: AuthError) {
        //Called when SwiftyVK could not authorize. To let the application know that something went wrong.
        print("Autorization failed with error: \n\(error)")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "TestVkDidNotAuthorize"), object: nil)
    }
    
    func vkDidUnauthorize() {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(vkTokenFile)
            let responseString = ""
            
            // Сохранение в файл
            do {
                try responseString.write(to: path, atomically: false, encoding: String.Encoding.utf8)
            } catch {
                print("Ошибка при сохранении в файл")
            }
        }
    }
    
    func vkShouldUseTokenPath() -> String? {
        // ---DEPRECATED. TOKEN NOW STORED IN KEYCHAIN---
        //Called when SwiftyVK need know where a token is located.
        //Path to save/read token or nil if should save token to UserDefaults
        return nil
    }
    
    func vkWillPresentView() -> UIViewController {
        //Only for iOS!
        //Called when need to display a view from SwiftyVK.
        //UIViewController that should present authorization view controller
        return UIApplication.shared.delegate!.window!!.rootViewController!
    }
}
