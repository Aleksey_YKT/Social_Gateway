//
//  RegistrationViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 30.11.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func confirmButtonPressed(_ sender: AnyObject) {
        view.endEditing(true)
        
        let userEmail = emailTextField.text!
        let userLogin = loginTextField.text!
        let userPassword = MD5(passwordTextField.text!)
        
        if (userEmail != "" && userLogin != "" && userPassword != "") {
            registerNewUser(userEmail: userEmail, userLogin: userLogin, userPassword: userPassword)
        } else {
            let alertMessage = UIAlertController(title: "Ошибка", message: "Заполните поля", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertMessage, animated: true, completion: nil)
        }
    }
    
    func registerNewUser(userEmail: String, userLogin: String, userPassword: String) {
        let requestParams = "name=" + userLogin + "&password=" + userPassword + "&email=" + userEmail
        
        var request = URLRequest(url: URL(string: registerNewUserRequestURL)!)
        request.httpMethod = "POST"
        let postString = requestParams
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Ошибка на сервере \(error)")
                
                let alertMessage = UIAlertController(title: "Ошибка", message: "Ошибка на сервере \(error)", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("Ошибка на сервере \(httpStatus.statusCode)")
                
                let alertMessage = UIAlertController(title: "Ошибка", message: "Ошибка на сервере \(httpStatus.statusCode)", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                print("Ваш запрос отправлен")
                
                let responseString = String(data: data, encoding: .utf8)
                print(responseString as String!)
                
                DispatchQueue.main.async {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "showLoginView", sender: self)
                    }
                }
            }
        }
        
        task.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "registerConfirmed" {
            let destinationController = segue.destination as! LoginViewController
            
            destinationController.needToHideBackButtom = true
        }
    }
}
