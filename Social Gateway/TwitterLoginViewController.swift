//
//  TwitterLoginViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 03.12.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit
import TwitterKit

class TwitterLoginViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logInButton = TWTRLogInButton { (session, error) in
            if let unwrappedSession = session {
                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                    let path = dir.appendingPathComponent(twitterTokenFile)
                    let responseString = unwrappedSession.authToken
                    
                    // Сохранение в файл
                    do {
                        try responseString.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                        print("Токен сохранен")
                    } catch {
                        print("Ошибка при сохранении в файл")
                    }
                }
                
                DispatchQueue.main.async {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "backToSettingsView", sender: self)
                    }
                }
            } else {
                NSLog("Login error: %@", error!.localizedDescription);
            }
        }
        
        // TODO: Change where the log in button is positioned in your view
        logInButton.center = self.view.center
        self.view.addSubview(logInButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

