//
//  User.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 03.12.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

class User {
    var userLogin: String = ""
    var userName: String = ""
    var userPass: String = ""
    var userPhoto: String = ""
    var userEmail: String = ""
    var userVKToken: String = ""
    var userTwitterToken: String = ""
}
