//
//  StartViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 30.11.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
