//
//  MD5.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 05.12.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

public func MD5(_ string: String) -> String {
    guard let messageData = string.data(using:String.Encoding.utf8) else { return "" }
    var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
    
    _ = digestData.withUnsafeMutableBytes {digestBytes in
        messageData.withUnsafeBytes {messageBytes in
            CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
        }
    }
    
    let md5Hex =  digestData.map { String(format: "%02hhx", $0) }.joined()
    
    return md5Hex
}
