//
//  RequestURLs.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 30.11.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

public let appID = "5755955"
public let vkTokenFile = "user_vk_token.txt"
public let twitterTokenFile = "user_twitter_token.txt"
public let appUserFile = "user.txt"

public let serverURL = "http://83.220.168.46:1555/"

public let registerNewUserRequestURL = serverURL + "api/users/add"
public let getUsersURL = serverURL + "api/users/get"

public let getPostsRequestURL = serverURL + "api/posts"

public let subscribeToProviderURL = serverURL + "api/subscribres/subscribe"
public let unsubscribeToProviderURL = serverURL + "api/subscribres/unsubscribe"
public let getSubscribedProvidersURL = serverURL + "api/subscribres/get"

public let addNewProviderURL = serverURL + "api/providers/add"
public let getPostsProviderRequestURL = serverURL + "api/providers"

public let emptyPhotoURL = "http://www.coptima.ru/bitrix/templates/coptima/images/cfdb4acd53b00bfad880e28dd08f8976.gif"
public let checkMark = "✓"
