//
//  AddProviderViewController.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 05.12.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class AddProviderViewController: UIViewController {

    @IBOutlet weak var serviceSegmentedControl: UISegmentedControl!
    @IBOutlet weak var serviceIDTextField: UITextField!
    
    var serviceID = 2 // 1 - Twitter, 2 - VK
    var providerID = ""
    var needMinus = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func segmentedControlAction(sender: AnyObject) {
        if (serviceSegmentedControl.selectedSegmentIndex == 0) {
            needMinus = true
            serviceID = 2
        } else if (serviceSegmentedControl.selectedSegmentIndex == 1) {
            needMinus = false
            serviceID = 2
        } else if (serviceSegmentedControl.selectedSegmentIndex == 2) {
            needMinus = false
            serviceID = 1
        }
    }
    
    @IBAction func confirmButtonPressed(_ sender: Any) {
        providerID = serviceIDTextField.text!
        
        if needMinus {
            providerID = "-" + providerID
        }
        
        addNewProviderRequest(serviceID: serviceID, providerID: providerID)
    }
    
    func addNewProviderRequest(serviceID: Int, providerID: String) {
        let requestParams = "owner_id=" + providerID + "&service_id=" + String(serviceID)
        var request = URLRequest(url: URL(string: addNewProviderURL)!)
        
        request.httpMethod = "POST"
        let postString = requestParams
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let _ = data, error == nil else {
                print("Ошибка на сервере \(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("Ошибка на сервере \(httpStatus.statusCode)")
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                print("Ваш запрос отправлен")
            }
        }
        
        task.resume()
    }
}
