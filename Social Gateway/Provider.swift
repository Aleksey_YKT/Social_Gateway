//
//  Provider.swift
//  Social Gateway
//
//  Created by Aleksey Ivanov on 28.11.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

class Provider {
    var id: String = ""
    var title: String = ""
    var logo: String = ""
    var description: String = ""
    var source_link: String = ""
    
    var service_name: String = ""
    var service_logo: String = ""
    
    var user_subscribed: Bool = false
}
